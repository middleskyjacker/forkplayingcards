
// Playing Cards
// Forked from Ethan Corrigan

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum class Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

enum class Suit
{
	Diamonds,
	Hearts,
	Spades,
	Clubs
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card) {
	cout << "The " << card.rank << " of " << card.suit << ".";
}

Card HighCard(Card card1, Card card2) {
	if (card1.rank > card2.rank) {
		return card1;
	}
	else {
		return card2;
	}
}

int main()
{
	Card c1;
	c1.rank = Rank::Ten;
	c1.suit = Suit::Hearts;

	Card c2;
	c2.rank = Rank::Ace;

	PrintCard(c1);

	(void)_getch();
	return 0;
}